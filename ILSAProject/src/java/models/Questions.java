/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "questions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questions.findAll", query = "SELECT q FROM Questions q"),
    @NamedQuery(name = "Questions.findByPk", query = "SELECT q FROM Questions q WHERE q.pk = :pk"),
    @NamedQuery(name = "Questions.findByQcode", query = "SELECT q FROM Questions q WHERE q.qcode = :qcode"),
    @NamedQuery(name = "Questions.findByQuestion", query = "SELECT q FROM Questions q WHERE q.question = :question"),
    @NamedQuery(name = "Questions.findByOpt1", query = "SELECT q FROM Questions q WHERE q.opt1 = :opt1"),
    @NamedQuery(name = "Questions.findByOpt2", query = "SELECT q FROM Questions q WHERE q.opt2 = :opt2"),
    @NamedQuery(name = "Questions.findByOpt3", query = "SELECT q FROM Questions q WHERE q.opt3 = :opt3"),
    @NamedQuery(name = "Questions.findByOpt4", query = "SELECT q FROM Questions q WHERE q.opt4 = :opt4"),
    @NamedQuery(name = "Questions.findByAnswer", query = "SELECT q FROM Questions q WHERE q.answer = :answer"),
    @NamedQuery(name = "Questions.findByBackground", query = "SELECT q FROM Questions q WHERE q.background = :background"),
    @NamedQuery(name = "Questions.findByTimelimit", query = "SELECT q FROM Questions q WHERE q.timelimit = :timelimit"),
    @NamedQuery(name = "Questions.findByPoints", query = "SELECT q FROM Questions q WHERE q.points = :points"),
    @NamedQuery(name = "Questions.findByLevel", query = "SELECT q FROM Questions q WHERE q.level = :level")})
public class Questions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk")
    private Integer pk;
    @Column(name = "qcode")
    private Integer qcode;
    @Size(max = 1000)
    @Column(name = "question")
    private String question;
    @Size(max = 500)
    @Column(name = "opt1")
    private String opt1;
    @Size(max = 500)
    @Column(name = "opt2")
    private String opt2;
    @Size(max = 500)
    @Column(name = "opt3")
    private String opt3;
    @Size(max = 500)
    @Column(name = "opt4")
    private String opt4;
    @Size(max = 500)
    @Column(name = "answer")
    private String answer;
    @Size(max = 500)
    @Column(name = "background")
    private String background;
    @Column(name = "timelimit")
    private Integer timelimit;
    @Column(name = "points")
    private Integer points;
    @Size(max = 45)
    @Column(name = "level")
    private String level;

    public Questions() {
    }

    public Questions(Integer pk) {
        this.pk = pk;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public Integer getQcode() {
        return qcode;
    }

    public void setQcode(Integer qcode) {
        this.qcode = qcode;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOpt1() {
        return opt1;
    }

    public void setOpt1(String opt1) {
        this.opt1 = opt1;
    }

    public String getOpt2() {
        return opt2;
    }

    public void setOpt2(String opt2) {
        this.opt2 = opt2;
    }

    public String getOpt3() {
        return opt3;
    }

    public void setOpt3(String opt3) {
        this.opt3 = opt3;
    }

    public String getOpt4() {
        return opt4;
    }

    public void setOpt4(String opt4) {
        this.opt4 = opt4;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getTimelimit() {
        return timelimit;
    }

    public void setTimelimit(Integer timelimit) {
        this.timelimit = timelimit;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pk != null ? pk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questions)) {
            return false;
        }
        Questions other = (Questions) object;
        if ((this.pk == null && other.pk != null) || (this.pk != null && !this.pk.equals(other.pk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Questions[ pk=" + pk + " ]";
    }
    
}
