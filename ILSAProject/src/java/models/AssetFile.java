/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "assetfile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AssetFile.findAll", query = "SELECT a FROM AssetFile a"),
    @NamedQuery(name = "AssetFile.findById", query = "SELECT a FROM AssetFile a WHERE a.id = :id"),
    @NamedQuery(name = "AssetFile.findByFileName", query = "SELECT a FROM AssetFile a WHERE a.fileName = :fileName"),
    @NamedQuery(name = "AssetFile.findByFileType", query = "SELECT a FROM AssetFile a WHERE a.fileType = :fileType"),
    @NamedQuery(name = "AssetFile.findByDateCreated", query = "SELECT a FROM AssetFile a WHERE a.dateCreated = :dateCreated"),
    @NamedQuery(name = "AssetFile.findByFileTypeString", query = "SELECT a FROM AssetFile a WHERE a.fileTypeString = :fileTypeString"),
    @NamedQuery(name = "AssetFile.findByFileSize", query = "SELECT a FROM AssetFile a WHERE a.fileSize = :fileSize"),
    @NamedQuery(name = "AssetFile.findByFilePath", query = "SELECT a FROM AssetFile a WHERE a.filePath = :filePath")})
public class AssetFile implements Serializable {
    @Size(max = 128)
    @Column(name = "CONTENT_TYPE")
    private String contentType;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 512)
    @Column(name = "FILE_NAME")
    private String fileName;
    @Size(max = 45)
    @Column(name = "FILE_TYPE")
    private String fileType;
    @Column(name = "DATE_CREATED")
    @Temporal(TemporalType.DATE)
    private Date dateCreated;
    @Size(max = 45)
    @Column(name = "FILE_TYPE_STRING")
    private String fileTypeString;
    @Column(name = "FILE_SIZE")
    private Integer fileSize;
    @Size(max = 1024)
    @Column(name = "FILE_PATH")
    private String filePath;

    public AssetFile() {
    }

    public AssetFile(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFileTypeString() {
        return fileTypeString;
    }

    public void setFileTypeString(String fileTypeString) {
        this.fileTypeString = fileTypeString;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssetFile)) {
            return false;
        }
        AssetFile other = (AssetFile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.AssetFile[ id=" + id + " ]";
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
}
