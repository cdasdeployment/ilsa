/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andoueili
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Members.findAll", query = "SELECT m FROM Members m"),
    @NamedQuery(name = "Members.findById", query = "SELECT m FROM Members m WHERE m.id = :id"),
    @NamedQuery(name = "Members.findByLname", query = "SELECT m FROM Members m WHERE m.lname = :lname"),
    @NamedQuery(name = "Members.findByFname", query = "SELECT m FROM Members m WHERE m.fname = :fname"),
    @NamedQuery(name = "Members.findByMname", query = "SELECT m FROM Members m WHERE m.mname = :mname"),
    @NamedQuery(name = "Members.findByBdate", query = "SELECT m FROM Members m WHERE m.bdate = :bdate"),
    @NamedQuery(name = "Members.findByPk", query = "SELECT m FROM Members m WHERE m.pk = :pk")})
public class Members implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String lname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String fname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    private String mname;
    @Basic(optional = false)
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date bdate;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer pk;

    public Members() {
    }

    public Members(Integer pk) {
        this.pk = pk;
    }

    public Members(Integer pk, String id, String lname, String fname, String mname, Date bdate) {
        this.pk = pk;
        this.id = id;
        this.lname = lname;
        this.fname = fname;
        this.mname = mname;
        this.bdate = bdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Date getBdate() {
        return bdate;
    }

    public void setBdate(Date bdate) {
        this.bdate = bdate;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pk != null ? pk.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Members)) {
            return false;
        }
        Members other = (Members) object;
        if ((this.pk == null && other.pk != null) || (this.pk != null && !this.pk.equals(other.pk))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return lname +", " + fname ;
    }
    
}
