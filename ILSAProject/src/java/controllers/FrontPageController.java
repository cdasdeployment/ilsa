/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import jpa.controller.MembersJpaController;
import jpa.controller.exceptions.RollbackFailureException;
import models.Members;

/**
 *
 * @author andoueili
 */
@ManagedBean(name = "frontPageController")
@ViewScoped
public class FrontPageController implements Serializable {

    @PersistenceUnit(unitName = "Project01PU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String ID = "";
    private String lname = "";
    private String fname = "";
    private String mname = "";
    private Date bdate = new Date();

    private List<Members> memberList = new ArrayList();
    private Members selectedMember;
    private String searchKey = "";
    private String selectName="";
            

    @PostConstruct
    public void init() {
        MembersJpaController mcontroller = new MembersJpaController(utx, factory);
        memberList = mcontroller.findMembersEntities();
    }

    public void editMember() {
        if (selectedMember != null) {
            EntityManager em = factory.createEntityManager();
            selectedMember = em.merge(selectedMember);
            em.refresh(selectedMember);
            em.close();
        }
    }

    public void seachMember() {
        MembersJpaController mcontroller = new MembersJpaController(utx, factory);
        memberList.clear();
        if (searchKey.isEmpty()) {
            memberList = mcontroller.findMembersEntities();
        } else {
            for (Members m : mcontroller.findMembersEntities()) {
                if (m.getLname().toLowerCase().contains(searchKey.toLowerCase()) || m.getFname().toLowerCase().contains(searchKey.toLowerCase())) {
                    memberList.add(m);
                }
            }
        }

    }

    public void commitUpdate() {
        if (selectedMember != null) {
            MembersJpaController mcontroller = new MembersJpaController(utx, factory);
            try {
                mcontroller.edit(selectedMember);

                memberList = mcontroller.findMembersEntities();  // to refresh the membmerlist which results into the refresh of the datatable in the form

                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Notification", "Update successful");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(FrontPageController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(FrontPageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the mname
     */
    public String getMname() {
        return mname;
    }

    /**
     * @param mname the mname to set
     */
    public void setMname(String mname) {
        this.mname = mname;
    }

    /**
     * @return the bdate
     */
    public Date getBdate() {
        return bdate;
    }

    /**
     * @param bdate the bdate to set
     */
    public void setBdate(Date bdate) {
        this.bdate = bdate;
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    public void submitPersonalInfo() {
        String s = "";
        s = ID + " " + lname + " " + fname + " " + mname + " " + bdate;
        System.out.println(s);

        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Notification", s);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        Members m = new Members();
        m.setId(ID);
        m.setLname(lname);
        m.setFname(fname);
        m.setMname(mname);
        m.setBdate(bdate);

        MembersJpaController mcontroller = new MembersJpaController(utx, factory);
        try {
            mcontroller.create(m);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(FrontPageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FrontPageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        memberList = mcontroller.findMembersEntities();
        
        ID = "";
        lname = "";
        mname = "";
        fname = "";
        bdate = new Date();

    }

    /**
     * @return the memberList
     */
    public List<Members> getMemberList() {
        return memberList;
    }

    /**
     * @param memberList the memberList to set
     */
    public void setMemberList(List<Members> memberList) {
        this.memberList = memberList;
    }

    /**
     * @return the selectedMember
     */
    public Members getSelectedMember() {
        return selectedMember;
    }

    /**
     * @param selectedMember the selectedMember to set
     */
    public void setSelectedMember(Members selectedMember) {
        this.selectedMember = selectedMember;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }

    /**
     * @param searchKey the searchKey to set
     */
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    /**
     * @return the selectName
     */
    public String getSelectName() {
        return selectName;
    }

    /**
     * @param selectName the selectName to set
     */
    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }
}
