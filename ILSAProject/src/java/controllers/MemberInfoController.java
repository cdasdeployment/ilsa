/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Members;

/**
 *
 * @author andoueili
 */
@ManagedBean(name = "memberInfoController")
@ViewScoped
public class MemberInfoController implements Serializable{

    @PersistenceUnit(unitName = "Project01PU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Members selectedMember;
    
    @PostConstruct
    public void init(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map<String,String> param = facesContext.getExternalContext().getRequestParameterMap();
        if(param.containsKey("viewId")){
            String memberId = param.get("viewId");
            EntityManager em = factory.createEntityManager();
            selectedMember = em.find(Members.class, Integer.parseInt(memberId));
            selectedMember = em.merge(selectedMember);
            em.refresh(selectedMember);
            em.close();
        }
    }

    /**
     * @return the selectedMember
     */
    public Members getSelectedMember() {
        return selectedMember;
    }

    /**
     * @param selectedMember the selectedMember to set
     */
    public void setSelectedMember(Members selectedMember) {
        this.selectedMember = selectedMember;
    }
}
