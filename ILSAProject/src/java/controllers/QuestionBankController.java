/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import jpa.controller.QuestionsJpaController;
import models.AssetFile;
import models.Questions;
import org.primefaces.event.FileUploadEvent;
import util.AppConfig;
import util.JsfUtil;

/**
 *
 * @author USER
 */
@ManagedBean(name = "questionBankController")
@ViewScoped
public class QuestionBankController implements Serializable {

    @PersistenceUnit(unitName = "Project01PU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private List<Questions> questionList = new ArrayList();
    private Questions selectedQuestion;

    private AssetFile assetFile;
    private String fileName = "";
    private String description = "";

    @PostConstruct
    public void init() {
        QuestionsJpaController questionController = new QuestionsJpaController(utx, factory);
        questionList = questionController.findQuestionsEntities();
    }

    /**
     * @return the questionList
     */
    public List<Questions> getQuestionList() {
        return questionList;
    }

    /**
     * @param questionList the questionList to set
     */
    public void setQuestionList(List<Questions> questionList) {
        this.questionList = questionList;
    }

    /**
     * @return the selectedQuestion
     */
    public Questions getSelectedQuestion() {
        return selectedQuestion;
    }

    /**
     * @param selectedQuestion the selectedQuestion to set
     */
    public void setSelectedQuestion(Questions selectedQuestion) {
        this.selectedQuestion = selectedQuestion;
    }

    public void edit() {
        if (selectedQuestion != null) {
            System.out.println(selectedQuestion.getQuestion());
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        if (fileName.trim().isEmpty()){
            return;
        }
        try {
            String destination = AppConfig.assetFilesDirectory + selectedQuestion.getPk() + File.separator;
            File des = new File(destination);
            if (!des.exists()) {
                des.mkdirs();
            }
            assetFile = new AssetFile();
            assetFile.setDateCreated(new Date());
            assetFile.setFileName("temp");
            assetFile.setFileSize((int) event.getFile().getSize());
            assetFile.setContentType(event.getFile().getContentType());

            AssetFileJpaController assetController = new AssetFileJpaController(utx, factory);
            assetController.create(assetFile);

            assetFile.setFileName(fileName);
            assetFile.setFilePath(destination + fileName);
            
            assetController.edit(assetFile);

        } catch (Exception ex) {
            JsfUtil.addErrorMessage("Error on uploading: " + event.getFile().getFileName() + "! " + ex.getMessage());
            Logger.getLogger(QuestionBankController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
