package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-03-10T12:34:56")
@StaticMetamodel(Members.class)
public class Members_ { 

    public static volatile SingularAttribute<Members, String> fname;
    public static volatile SingularAttribute<Members, String> lname;
    public static volatile SingularAttribute<Members, Date> bdate;
    public static volatile SingularAttribute<Members, String> id;
    public static volatile SingularAttribute<Members, String> mname;
    public static volatile SingularAttribute<Members, Integer> pk;

}