package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-03-10T12:34:56")
@StaticMetamodel(Questions.class)
public class Questions_ { 

    public static volatile SingularAttribute<Questions, String> question;
    public static volatile SingularAttribute<Questions, String> opt3;
    public static volatile SingularAttribute<Questions, String> answer;
    public static volatile SingularAttribute<Questions, String> level;
    public static volatile SingularAttribute<Questions, String> opt4;
    public static volatile SingularAttribute<Questions, String> background;
    public static volatile SingularAttribute<Questions, Integer> qcode;
    public static volatile SingularAttribute<Questions, String> opt1;
    public static volatile SingularAttribute<Questions, String> opt2;
    public static volatile SingularAttribute<Questions, Integer> pk;
    public static volatile SingularAttribute<Questions, Integer> timelimit;
    public static volatile SingularAttribute<Questions, Integer> points;

}